package com.test.kelas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.kelas.entity.Kelas;
import com.test.kelas.repository.KelasRepository;

@Service
public class KelasService {

	@Autowired
	private KelasRepository kelasRepository;

	public List<Kelas> getAllKelas() {
		return kelasRepository.findAll();
	}

	public Kelas getKelasById(Long id) {
		return kelasRepository.findById(id).orElse(null);
	}

	public Kelas saveKelas(Kelas kelas) {
		return kelasRepository.save(kelas);
	}

	public void deleteKelas(Long id) {
		kelasRepository.deleteById(id);
	}

}
