package com.test.kelas.controller;

import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.kelas.entity.Kelas;
import com.test.kelas.service.KelasService;

@RestController
@RequestMapping("/api/class")
public class KelasController {

	@Autowired
	private KelasService kelasService;

	private Logger log = LogManager.getLogger(KelasController.class);

	@GetMapping
	public List<Kelas> getAllKelas() {
		return kelasService.getAllKelas();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Kelas> getKelasById(@PathVariable Long id) {
		return ResponseEntity.ok(kelasService.getKelasById(id));
	}

	@PostMapping
	public ResponseEntity<Kelas> createKelas(@Valid @RequestBody Kelas kelas) throws ParseException {

		log.info("=============== CREATE CLASS ===============");

		kelasService.saveKelas(kelas);
		log.info("Insert database done");
		log.info("=============== END CREATE CLASS ===============");

		return new ResponseEntity<>(kelas, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteKelas(@PathVariable Long id) {
		log.info("=============== DELETE CLASS ===============");

		kelasService.deleteKelas(id);

		log.info("Done delete class, id : " + id);
		log.info("=============== END DELETE CLASS ===============");

		return ResponseEntity.noContent().build();
	}
}
