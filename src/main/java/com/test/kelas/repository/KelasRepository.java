package com.test.kelas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.kelas.entity.Kelas;

public interface KelasRepository extends JpaRepository<Kelas, Long> {

}
